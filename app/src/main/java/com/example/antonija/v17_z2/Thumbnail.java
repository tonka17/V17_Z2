package com.example.antonija.v17_z2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Antonija on 17/09/2017.
 */
@Root(name = "enclosure", strict = false)
class Thumbnail {
    @Attribute(name = "url") String _URL;

    public Thumbnail() {
    }

    public Thumbnail(String URL) {
        this._URL = URL;
    }

    public String getURL() {
        return _URL;
    }
}
