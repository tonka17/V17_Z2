package com.example.antonija.v17_z2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;

/**
 * Created by Antonija on 17/09/2017.
 */

public interface BugAPI {
    static String BASE_URL = "http://www.bug.hr/";
    @GET("rss/vijesti")
    Call<Feed> getNews();
}
