package com.example.antonija.v17_z2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemSelected;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MainActivity extends Activity implements Callback<Feed> {
    private static final String BASE_URL = "http://www.bug.hr/";
    @BindView(R.id.lvBugNews)
    ListView lvBugNews;
    @BindView(R.id.etCategory)
    EditText etCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadNews();
    }


    private void loadNews() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();


        BugAPI api = retrofit.create(BugAPI.class);
        Call<Feed> call = api.getNews();
        call.enqueue(this);
    }



    @Override
    public void onResponse(Call<Feed> call, Response<Feed> response) {
        Channel feed = response.body().getchannel();
        List<FeedItem> items = feed.getItems();
        String chosenCategory = etCategory.getText().toString();
        loadNews();
        for (int i = 0; i < items.size(); i++) {
            if(chosenCategory.equals("")){
                break;
            }
            else if (!chosenCategory.equals(items.get(i).getcategory())){
                items.remove(i);
                i--;
            }
        }

        NewsAdapter adapter = new NewsAdapter(items);
        lvBugNews.setAdapter(adapter);

    }

    @Override
    public void onFailure(Call<Feed> call, Throwable t) {
        Log.e("Fail", t.getMessage());
    }

    @OnItemClick(R.id.lvBugNews)
    public void OpenLink(int position)
    {
        FeedItem feeditem = (FeedItem) this.lvBugNews.getAdapter().getItem(position);
        String link = feeditem.getlink();
        Uri data = Uri.parse(link);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(data);
        if(canBeCalled(intent))
        {
            startActivity(intent);
        }
    }

    private boolean canBeCalled(Intent intent) {
        PackageManager pm = this.getPackageManager();
        if(intent.resolveActivity(pm)==null)
        {
            return  false;
        }
        else
        {
            return true;
        }
    }
}
