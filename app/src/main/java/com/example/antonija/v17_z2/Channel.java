package com.example.antonija.v17_z2;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Antonija on 17/09/2017.
 */
@Root(name = "channel", strict = false)
public class Channel {
    @ElementList(name = "item", inline = true) List<FeedItem> _items;

    public Channel() {
    }

    public Channel(List<FeedItem> items) {
        this._items = items;
    }

    public List<FeedItem> getItems() {
        return _items;
    }
}
