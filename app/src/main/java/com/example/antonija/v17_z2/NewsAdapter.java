package com.example.antonija.v17_z2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 17/09/2017.
 */

public class NewsAdapter extends BaseAdapter {

    List<FeedItem> _NewsItems;

    public NewsAdapter(List<FeedItem> newsItems) {
        this._NewsItems = newsItems;
    }

    @Override
    public int getCount() {
        return this._NewsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this._NewsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsHolder holder;
        if(convertView==null)
        {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            holder = new NewsHolder(convertView);
            convertView.setTag(holder);
        }
        else
        {
            holder = (NewsHolder) convertView.getTag();
        }

        FeedItem newsItem = this._NewsItems.get(position);
        holder.tvNewsTitle.setText(newsItem.gettitle());
        holder.tvDate.setText(newsItem.getpubDate());
        holder.tvCategory.setText(newsItem.getcategory());
        Picasso.with(parent.getContext()).load(newsItem.getthumbnail().getURL()).fit().centerCrop().placeholder(R.drawable.loading).into(holder.ivThumbnail);

        return convertView;
    }
    static class NewsHolder
    {
        @BindView(R.id.tvNewsTitle)
        TextView tvNewsTitle;
        @BindView(R.id.tvDate) TextView tvDate;
        @BindView(R.id.ivThumbnail)
        ImageView ivThumbnail;
        @BindView(R.id.tvCategory) TextView tvCategory;

        public NewsHolder (View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
