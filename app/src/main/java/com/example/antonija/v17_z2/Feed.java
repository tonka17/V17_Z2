package com.example.antonija.v17_z2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Antonija on 17/09/2017.
 */
@Root(name = "rss", strict = false)
class Feed {
    @Element(name = "channel") Channel _channel;

    public Feed() {
    }

    public Feed(Channel channel) {
        this._channel = channel;
    }

    public Channel getchannel() {
        return _channel;
    }
}
