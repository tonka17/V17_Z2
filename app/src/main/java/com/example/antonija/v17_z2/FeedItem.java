package com.example.antonija.v17_z2;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Created by Antonija on 17/09/2017.
 */
@Root(name = "item", strict = false)
class FeedItem {
    @Element(name = "title") String _title;
    @Element(name = "link") String _link;
    @Element(name = "pubDate") String _pubDate;
    @Element(name = "enclosure") Thumbnail _thumbnail;
    @Element(name = "category") String _category;

    public FeedItem() {
    }

    public FeedItem(String title, String link, String pubDate, Thumbnail thumbnail, String category) {
        this._title = title;
        this._link = link;
        this._pubDate = pubDate;
        this._thumbnail = thumbnail;
        this._category = category;
    }

    public String gettitle() {
        return _title;
    }

    public String getlink() {
        return _link;
    }

    public String getpubDate() {
        return _pubDate;
    }

    public Thumbnail getthumbnail() {
        return _thumbnail;
    }

    public String getcategory() {
        return _category;
    }
}
